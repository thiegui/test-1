 $(function(){
	    $("#hotel_roma").tooltip();
	    $("[data-toggle='popover']").popover();
	    $('.carousel').carousel({
		    interval:2000
	    });
	    $('#contactoModal').on('show.bs.modal', function(e){ 
		    console.log('show');   
		    $('#contacto').removeClass('btn-primary').addClass('btn-outline-success');
	    });
	    $('#contactoModal').on('shown.bs.modal', function(e){ console.log('shown');   
		$('#contacto').prop('disabled', true);
	    });
	    $('#contactoModal').on('hide.bs.modal', function(e){ console.log('hide');   
		    $('#contacto').addClass('btn-primary').removeClass('btn-outline-success');

		});
	    $('#contactoModal').on('hidden.bs.modal', function(e){ console.log('hidden');   

		$('#contacto').prop('disabled', false);
	    });

		
    });