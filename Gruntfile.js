module.exports = function(grunt){
	require('time-grunt')(grunt);
	require('jit-grunt')(grunt,{
		useminPrepare: 'grunt-usemin'
	});

	grunt.initConfig({
		sass:{
			dist:{
				files:[{
					expand:true,
					cwd:'scss',
					src:['*.scss'],
					dest:'css',
					ext:'.css'	
					}]
				}
		},
		watch:{
			files: ['scss/*.scss'],
			tasks: ['css']
		},
		browserSync:{
			dev:{
				bsFiles:{
					src:['css/*.css','*.html','js/*.js']
				}
			},
			options:{
				watchTask: true,
				server:{ baseDir: './'}	
			}
		},
		imagemin:{
			dynamic:{
				files : [{
					expand: true,
					cwd: './',
					src:['images/*.{png,gif,jpg,jpeg}','images/carousel/*.{png,gif,jpg,jpeg}'],
					dest: 'dist/'
				}]
			}
		},
		copy:{
			html:{
				files: [{
					expand:true,
					dot:true,
					cwd: './', 
					src: ['*.html'],
					dest:'dist'
				}]
			},
			fonts:{
				files:[{
					expand:true,
					dot: true,
					cwd: 'node_modules/open-iconic/font',
					src: ['fonts/*.*'],
					dest : 'dist'
				}]
			}

		},
		clean:{
			build:{
				src: ['dist/']
			}
		},
		cssmin:{
			dist:{}
		},
		uglify:{
			dist:{}
		},
		filerev:{
			options:{
				algorithm: 'md5',
				length: 15
			},
			files:{
				src:['dist/css/*.css', 'dist/js/*.js']
			}
		},
		concat:{
			options:{
				separator: ';'
			},
			dist: {}
		},
		useminPrepare:{
			foo:{
				dest:'dist',
				src:[ 'index.html', 'contact.html', 'nosotros.html', 'precios.html']
			},
			options:{
				flow:{
					steps: {
						css : ['cssmin'],
						js : ['uglify']
					}
				},
				post:{
					css:[{
						name : 'cssmin',
						createConfig: function(context,block){
							var generated = context.options.generated;
							generated.options = {
								keepSpecialComments : 0,
								rebase:false
							}
						}
					}]
				}
			}
			
		},
		usemin:{
			html : ['dist/index.html','dist/contact.html','dist/nosotros.html','dist/precios.html'],
			options:{
				assetsDir : ['dist', 'dist/css', 'dist/js']
			}
		}

	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-usemin');
	grunt.loadNpmTasks('grunt-filerev');
	
	grunt.registerTask('css', ['sass']);	
	grunt.registerTask('default', ['browserSync', 'watch']);
	grunt.registerTask('img:compress', ['imagemin']);	

	grunt.registerTask('build',[
		'clean', //borrar contenido dist
		'copy', //copiamos los html a dist
		'imagemin', //optimizamos imagenes
		'useminPrepare', //preparamos la configuración de usemin
		'concat',
		'cssmin',
		'uglify',
		'filerev',//agregamos cadena aletaria
		'usemin' //reemplazamos las referencias por los archivos generados por filerev
	]);
};

